# OpenML dataset: CPS1988

https://www.openml.org/d/43963

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Source**

This dataset was obtained from the **AER** R package (see citation request below).

**Data description**

Cross-section data originating from the March 1988 Current Population Survey by the US Census Bureau.
The data is a sample of men aged 18 to 70 with positive annual income greater than USD 50 in 1992, who
are not self-employed nor working without pay. Wages are deflated by the deflator of Personal
Consumption Expenditure for 1992.
A problem with CPS data is that it does not provide actual work experience. It is therefore customary
to compute experience as age - education - 6 (as was done by Bierens and Ginther, 2001), this
may be considered potential experience. As a result, some respondents have negative experience


**Attribute Information**

* wage Wage (in dollars per week).
* education Number of years of education.
* experience Number of years of potential work experience.
* ethnicity Factor with levels "cauc" and "afam" (African-American).
* smsa Factor. Does the individual reside in a Standard Metropolitan Statistical Area (SMSA)?
* region Factor with levels "northeast", "midwest", "south", "west".
* parttime Factor. Does the individual work part-time?


**Citation Request**

Christian Kleiber and Achim Zeileis (2008). Applied Econometrics with
R. New York: Springer-Verlag. ISBN 978-0-387-77316-2. URL
https://CRAN.R-project.org/package=AER

*Bibtex*

@book{kleiber2008applied,
  title={Applied econometrics with R},
  author={Kleiber, Christian and Zeileis, Achim},
  year={2008},
  publisher={Springer Science \& Business Media}
}

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43963) of an [OpenML dataset](https://www.openml.org/d/43963). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43963/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43963/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43963/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

